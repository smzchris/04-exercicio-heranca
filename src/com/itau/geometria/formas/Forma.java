package com.itau.geometria.formas;

public abstract class Forma {
	public abstract double calcularArea();
	
	public String toString() {
		return Double.toString(calcularArea());
	}
}
