package com.itau.geometria.formas;

public class Retangulo extends Forma{
	private double altura;
	private double largura;
	
	public Retangulo(double altura, double largura) {
		this.altura = altura;
		this.largura = largura;
	}

	public double getAltura() {
		return altura;
	}
	
	public double getLargura() {
		return largura;
	}

	@Override
	public double calcularArea() {
		return altura * largura;
	}
}
