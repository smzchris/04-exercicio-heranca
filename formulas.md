## Círculo
### Área
```
return Math.pow(raio, 2) * Math.PI;
```

## Retângulo
### Área
```
return altura * largura;
```

## Triângulo
### Triângulo válido
```
(ladoA + ladoB) > ladoC && (ladoA + ladoC) > ladoB && (ladoB + ladoC) > ladoA == true
```

### Área
```
double s = (ladoA + ladoB + ladoC) / 2;       
Math.sqrt(s * (s - ladoA) * (s - ladoB) * (s - ladoC))
```